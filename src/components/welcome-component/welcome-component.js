import React from "react";
import {
    Wrapper,
    StyledButtonLink
} from "../styled-components/styled-components";

class Welcome extends React.Component {
    render() {
        return (
            <div>
                <Wrapper>
                    <h1>Welcome to our Quiz App!!</h1>
                    <h3>You're Just a step away to experience Awesomeness.</h3>
                    <br />
                    <StyledButtonLink to="/1">
                        Click Here to Start Quiz
          </StyledButtonLink>
                </Wrapper>
            </div>
        );
    }
}

export default Welcome;
