import React from "react";
import { QuestionWrapper, HeadingWrapper, OptionWrapper } from '../styled-components/styled-components';

class Ques extends React.Component {
    handleAnswerChange = (event) => {
        const target = event.target
        const childIndex = target.value;
        const parentindex = target.getAttribute('data-parentindex');
        this.props.onSelectAnswer(childIndex, parentindex);

    }
    render() {
        const props = this.props;
        return (
            <div>
                <QuestionWrapper>
                    <HeadingWrapper>
                        <p>{`0${props.index + 1}_`}</p>
                        <p>{props.data.question}</p>
                    </HeadingWrapper>
                    <OptionWrapper>
                        {props.data.options.map((opt, i) => {
                            return (
                                <div key={i} >
                                    <input type="radio" name={`answer${props.index}`} value={i} data-parentindex={props.index}
                                        defaultChecked={props.data.selectedIndex === i}
                                        onChange={this.handleAnswerChange.bind(this)} />
                                    <span>{opt}</span>
                                </div>

                            )

                        })
                        }
                    </OptionWrapper>
                </QuestionWrapper>
            </div>
        );
    }
}

export default Ques;