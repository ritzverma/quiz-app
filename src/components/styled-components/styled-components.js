import styled, { css } from "styled-components";
import { Link } from "react-router-dom";

export const QuestionWrapper = styled.div`
    position: relative;
    width: 446px;
    height: auto;
    min-height: 280px;
    background-color:white;
    left:0;
    right:0;
    margin:0 auto;
    margin-top:80px;
    box-shadow: 0 10px 20px 0 rgba(0,0,0,0.20);
    @media only screen and (max-width: 768px) {
        width: 325px;
        left: 10px;
        right:15px;
        height: auto;
        min-height: 205px;
    }
`;

export const HeadingWrapper = styled.div`
    position: relative;
    width: 446px;
    height: auto;
    min-height: 80px;
    background-color: white;
    top: -30px;
    left: -40px;
    box-shadow: 0 10px 20px 0 rgba(0,0,0,0.20);
    text-align: center;

    >p:first-of-type{
        font-family: Gotham-Bold;
        font-size: 18px;
        color: #FFFFFF;
        -webkit-letter-spacing: 0;
        -moz-letter-spacing: 0;
        -ms-letter-spacing: 0;
        letter-spacing: 0;
        background: #0030FF;
        width: 80px;
        height: 80px;
        display: inline-block;
        float: left;
        margin: 0;
        line-height: 80px;
        @media only screen and (max-width: 768px) {
            line-height:50px;
            text-align:center;
            height:50px;
        }
    }
    >p{
        font-family: Gotham-Medium;
        font-size: 16px;
        color: #333333;
        letter-spacing: 0;
        line-height: 80px;
        width: 366px;
        margin :0
        @media only screen and (max-width: 768px) {
            line-height:50px;
            text-align:center;
            width: 295px;
        }

    }
    @media only screen and (max-width: 768px) {
        width: 315px;
        left: -18px;
        height: auto;
        min-height: 50px;
    }
    
`;

export const OptionWrapper = styled.div`
    padding-left:80px;
    font-family: Gotham-Medium;
    font-size: 16px;
    color: #333333;
    letter-spacing: 0;

    >div>input[type="radio"]{
        margin-right:15px;
    }
`;

export const Wrapper = styled.div`
  text-align: center;
`;

export const StyledButtonLink = styled(Link)`
  text-decoration: none;
  background-color: ${props => props.theme.blue};
  border: 1px solid ${props => props.theme.blue};
  color: ${props => props.theme.white};
  border-radius: 3px;
  padding: 10px 12px;
`;
export const FooterWrapper = styled.div`
    text-align: center;
    padding-top:30px;
    width: 446px;
    left:0;
    right:0;
    margin:0 auto;
    position: relative;
    >div{
        display: inline-block;
    }
    margin-bottom: 30px;
    @media only screen and (max-width: 768px) {
        width: 295px;
    }
`;

export const ChevImage = styled.img`{
    height:30px;
    position: absolute;
    ${props =>
        props.left &&
        css`
          left:0;
        `};
      ${props =>
        props.right &&
        css`
          right: 0;
        `}
}`

export const Dot = styled.span`
  font-size: 1.5em;
  text-shadow: 1px 1px 1px #fff;
  user-select: none;
`;
export const Dots = styled.span`
  text-align: center;
  width: 20px;
  z-index: 100;
`;
export const Button = styled.button`
  border-radius: 3px;
  border: none;
  color: #fff;
  cursor: pointer;
  font-size: 16px;
  padding: 10px 12px;
  text-align: center;
  background-color: ${props => props.theme.blue};
  border: 1px solid ${props => props.theme.blue};
`;
export const ButtonWrapper = styled.div`
    display: block !important;
    margin-top:10px;
`;

export const ModalBox = styled.div`
    position: fixed;
    z-index: 1;
    padding-top: 100px;
    left: 0;
    top: 0;
    width: 100%; 
    height: 100%;
    overflow: auto; 
    background-color: rgb(0,0,0);
    background-color: rgba(0,0,0,0.4);
`

export const ModalHeading = styled.div`
    margin-top:100px;
	height:88px;
	width:248px;
	margin-left:auto;
	margin-right:auto;
	display:block;
	background: #FFFFFF;
	position:relative;
    top:30px;
    >p{
        text-align:center;
        text-transform: uppercase;
        font-family: Gotham-Bold;
        font-size: 18px;
        color: #0030FF;
        letter-spacing: 6px;
        line-height: 88px;
    }
`;

export const ModalResult = styled.div`
    margin-left:auto;
    margin-right:auto;
    display:block;
    height:256px;
    width:321px;
    background: #0030FF;
    border: 8px solid #FFFFFF;
    >h2{
        font-family: Gotham-Bold;
        font-size: 42px;
        color: #FFFFFF;
        letter-spacing: 2px;
        text-align:center;
    }
    >p{
        font-family: Gotham-Medium;
        font-size: 16px;
        color: #FFFFFF;
        letter-spacing: 0;
        line-height: 20px;
        text-align:center;
    }
    img{
        height: 14px;
        margin-right:5px;
    }
`;