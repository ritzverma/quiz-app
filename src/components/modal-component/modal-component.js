import React from 'react';
import { ModalBox, ModalHeading, ModalResult } from '../styled-components/styled-components';
import CorrectAnswerImage from '../../assets/verification-mark.svg';
import WrongAnswerImage from '../../assets/cross-out-mark.svg';

class Modal extends React.Component {
    closeModal = () => {
        this.props.onClose();
    }
    render() {
        // Render nothing if the "show" prop is false
        if (!this.props.show) {
            return null;
        }
        const props = this.props;
        return (
            <ModalBox onClick={this.closeModal.bind(this)}>
                <ModalHeading>
                    <p>Your Score</p>
                </ModalHeading>

                <ModalResult>
                    <h2>{props.totalMarks}</h2>

                    <p>
                        <span>
                            <img src={CorrectAnswerImage} />
                        </span>
                        {props.totalRightAnswer} correct
                    </p>
                    <p>
                        <span>
                            <img src={WrongAnswerImage} />
                        </span>
                        {props.totalQuestion - props.totalRightAnswer} incorrect
                    </p>
                </ModalResult>
            </ModalBox>
        );
    }
}



export default Modal;
