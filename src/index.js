import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import registerServiceWorker from './registerServiceWorker';
import Routes from "./router";
import { ThemeProvider } from "styled-components";
import theme from "./themes/appTheme";


ReactDOM.render(
    <ThemeProvider theme={theme}>
        <Routes />
    </ThemeProvider>,
    document.getElementById('root'));
registerServiceWorker();
