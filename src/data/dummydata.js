const quiz = {
    "results": [
        {
            "gender": "male",
            "name": {
                "title": "mr",
                "first": "clinton",
                "last": "thomas"
            },
            "location": {
                "street": "9678 mockingbird ln",
                "city": "riverside",
                "state": "illinois",
                "postcode": 31711,
                "coordinates": {
                    "latitude": "88.9490",
                    "longitude": "-21.8237"
                },
                "timezone": {
                    "offset": "-8:00",
                    "description": "Pacific Time (US & Canada)"
                }
            },
            "email": "clinton.thomas@example.com",
            "login": {
                "uuid": "3e2f8b43-af1f-46db-baec-70bdc95e258e",
                "username": "tinypeacock531",
                "password": "oedipus",
                "salt": "P8e7A1T7",
                "md5": "33b02eb76b711f558f4117b571131957",
                "sha1": "9518a9feb9397665eba34d9ea55813025d15edde",
                "sha256": "1d3696ad8f6f85eb3f0a5910fd61b1d98041f63a62f2bd05ad50df8efc2ae141"
            },
            "dob": {
                "date": "1946-09-09T00:33:55Z",
                "age": 71
            },
            "registered": {
                "date": "2013-12-02T15:11:17Z",
                "age": 4
            },
            "phone": "(763)-813-5993",
            "cell": "(097)-780-6287",
            "id": {
                "name": "SSN",
                "value": "920-43-3224"
            },
            "picture": {
                "large": "https://randomuser.me/api/portraits/men/86.jpg",
                "medium": "https://randomuser.me/api/portraits/med/men/86.jpg",
                "thumbnail": "https://randomuser.me/api/portraits/thumb/men/86.jpg"
            },
            "nat": "US"
        },
        {
            "gender": "female",
            "name": {
                "title": "madame",
                "first": "jeanette",
                "last": "rey"
            },
            "location": {
                "street": "6475 rue dumenge",
                "city": "ebnat-kappel",
                "state": "aargau",
                "postcode": 7128,
                "coordinates": {
                    "latitude": "88.3981",
                    "longitude": "-59.8476"
                },
                "timezone": {
                    "offset": "-8:00",
                    "description": "Pacific Time (US & Canada)"
                }
            },
            "email": "jeanette.rey@example.com",
            "login": {
                "uuid": "1eccc0b3-cbf3-4076-a437-3e7d62ae30ed",
                "username": "bluebear634",
                "password": "deftones",
                "salt": "1boi26lC",
                "md5": "53680705e0b094e8767b24e7b1b95e0b",
                "sha1": "85622fa487558c682bfb28f652e290ff42e2dc36",
                "sha256": "c60c361e029741501d895adb13017a5cc86cfbefd61e0053adce8d5a89c0a9d3"
            },
            "dob": {
                "date": "1975-03-07T20:37:21Z",
                "age": 43
            },
            "registered": {
                "date": "2003-02-21T08:35:19Z",
                "age": 15
            },
            "phone": "(928)-842-4999",
            "cell": "(346)-106-7305",
            "id": {
                "name": "AVS",
                "value": "756.0929.4968.68"
            },
            "picture": {
                "large": "https://randomuser.me/api/portraits/women/31.jpg",
                "medium": "https://randomuser.me/api/portraits/med/women/31.jpg",
                "thumbnail": "https://randomuser.me/api/portraits/thumb/women/31.jpg"
            },
            "nat": "CH"
        },
        {
            "gender": "male",
            "name": {
                "title": "mr",
                "first": "claus-dieter",
                "last": "holzapfel"
            },
            "location": {
                "street": "buchenweg 29",
                "city": "marktleuthen",
                "state": "baden-württemberg",
                "postcode": 53675,
                "coordinates": {
                    "latitude": "-85.0391",
                    "longitude": "-96.4924"
                },
                "timezone": {
                    "offset": "+7:00",
                    "description": "Bangkok, Hanoi, Jakarta"
                }
            },
            "email": "claus-dieter.holzapfel@example.com",
            "login": {
                "uuid": "2de27430-9905-4faf-bde6-47fee679209b",
                "username": "purplebird690",
                "password": "rangers",
                "salt": "mnB1MIuP",
                "md5": "f1f78d9d1e1c25ca4fe79e5c988c1b41",
                "sha1": "9786708e1cf16822d226e7b6f5fc831ab03528de",
                "sha256": "b00c71ef103aed23dda59be145bf16ca2637526ece19b30c078ab1e2c84047bf"
            },
            "dob": {
                "date": "1978-12-15T10:23:45Z",
                "age": 39
            },
            "registered": {
                "date": "2017-09-28T15:30:50Z",
                "age": 0
            },
            "phone": "0240-6203810",
            "cell": "0175-7216156",
            "id": {
                "name": "",
                "value": null
            },
            "picture": {
                "large": "https://randomuser.me/api/portraits/men/37.jpg",
                "medium": "https://randomuser.me/api/portraits/med/men/37.jpg",
                "thumbnail": "https://randomuser.me/api/portraits/thumb/men/37.jpg"
            },
            "nat": "DE"
        },
        {
            "gender": "female",
            "name": {
                "title": "ms",
                "first": "solène",
                "last": "lambert"
            },
            "location": {
                "street": "5961 rue laure-diebold",
                "city": "besançon",
                "state": "alpes-de-haute-provence",
                "postcode": 79738,
                "coordinates": {
                    "latitude": "-20.7652",
                    "longitude": "-159.2681"
                },
                "timezone": {
                    "offset": "-1:00",
                    "description": "Azores, Cape Verde Islands"
                }
            },
            "email": "solène.lambert@example.com",
            "login": {
                "uuid": "90e4f7ea-dad4-4725-8e38-07336d985aba",
                "username": "angrylion349",
                "password": "stoned",
                "salt": "zdKwkKH1",
                "md5": "d15bf4fb167ccd5dfce6efae699cc536",
                "sha1": "2379a89ebc581b633beca5c18d29646bcf0f3774",
                "sha256": "0a218d36459b08ade5e68ec7bc604f4acf938bab9a28ceeb03f4f95d61187b5b"
            },
            "dob": {
                "date": "1978-10-05T06:15:36Z",
                "age": 39
            },
            "registered": {
                "date": "2012-10-06T23:02:23Z",
                "age": 5
            },
            "phone": "01-54-95-94-23",
            "cell": "06-14-37-63-41",
            "id": {
                "name": "INSEE",
                "value": "2NNaN72499149 22"
            },
            "picture": {
                "large": "https://randomuser.me/api/portraits/women/18.jpg",
                "medium": "https://randomuser.me/api/portraits/med/women/18.jpg",
                "thumbnail": "https://randomuser.me/api/portraits/thumb/women/18.jpg"
            },
            "nat": "FR"
        },
        {
            "gender": "male",
            "name": {
                "title": "mr",
                "first": "vaíse",
                "last": "araújo"
            },
            "location": {
                "street": "8556 rua vinte e dois ",
                "city": "governador valadares",
                "state": "rio grande do sul",
                "postcode": 55536,
                "coordinates": {
                    "latitude": "-76.5583",
                    "longitude": "56.0686"
                },
                "timezone": {
                    "offset": "-1:00",
                    "description": "Azores, Cape Verde Islands"
                }
            },
            "email": "vaíse.araújo@example.com",
            "login": {
                "uuid": "af546194-16e3-409c-b2a2-9a27de7b69e2",
                "username": "blueduck864",
                "password": "broken",
                "salt": "XIMKPteS",
                "md5": "9e6caa55ad5f7a90af3fe0e7404997a1",
                "sha1": "4d57537f398603b7eadd6411f056d7be06f3a15e",
                "sha256": "9c4d606dd9d99806b5462027d9a3670506c02ec1cd9640d9f3ab0fb20818ec99"
            },
            "dob": {
                "date": "1945-09-23T14:18:43Z",
                "age": 72
            },
            "registered": {
                "date": "2009-12-05T13:59:50Z",
                "age": 8
            },
            "phone": "(93) 7883-6478",
            "cell": "(42) 1598-4873",
            "id": {
                "name": "",
                "value": null
            },
            "picture": {
                "large": "https://randomuser.me/api/portraits/men/11.jpg",
                "medium": "https://randomuser.me/api/portraits/med/men/11.jpg",
                "thumbnail": "https://randomuser.me/api/portraits/thumb/men/11.jpg"
            },
            "nat": "BR"
        }
    ],
    "info": {
        "seed": "50a29c5f3d004f51",
        "results": 5,
        "page": 1,
        "version": "1.2"
    }
}