import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import App from './App';
import Welcome from './components/welcome-component/welcome-component';


const Routes = () => {
    return (
        <Router>
            <div>
                <Route path="/" exact component={Welcome} />
                <Route path="/:id" exact component={App} />
            </div>
        </Router>
    );
};

export default Routes;

