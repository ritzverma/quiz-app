import React, { Component } from 'react';

import axios from 'axios';
import Ques from './components/question-component/question-component';
import Modal from './components/modal-component/modal-component';
import { Link } from "react-router-dom";
import LeftIcon from "./assets/Chevron-Left.png";
import RightIcon from "./assets/Chevron-Right.png";
import { FooterWrapper, ChevImage, Dot, Dots, Button, ButtonWrapper } from './components/styled-components/styled-components';

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      questions: [],
      step: 1,
      countsperpage: 2,
      totalRightAnswer: 0,
      totalMarks: 0,
      isOpen: false,
      selectedAnswers: []
    }
    this.renderQuizData = this.renderQuizData.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.showResult = this.showResult.bind(this);
    this.renderButton = this.renderButton.bind(this);
  }

  renderQuizData = (data) => {
    const step = this.state.step;
    const countperpage = this.state.countsperpage;
    return (
      data.map((d, i) => {
        if (i >= countperpage * (step - 1) && i < countperpage * step) {
          return <Ques key={i} data={d} index={i} onSelectAnswer={this.handleChange}></Ques>
        }
      })
    )
  }
  toggleModal = () => {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  handleChange = (childindex, parentindex) => {
    const state = this.state;
    const questions = state.questions;
    const currentQuestion = questions[parentindex];
    currentQuestion.selectedIndex = parseInt(childindex);
    const selectObj = { id: currentQuestion.id, selectedAnswerIndex: currentQuestion.selectedIndex + 1 }
    if (state.selectedAnswers.some(elem => elem.id === currentQuestion.id)) {
      state.selectedAnswers = state.selectedAnswers.map((el) => {
        if (el.id === currentQuestion.id) {
          el.selectedAnswerIndex = currentQuestion.selectedIndex + 1;
        }
        return el;
      });
    }
    else {
      state.selectedAnswers.push(selectObj);
    }
  }
  showResult = () => {
    axios({
      method: 'post',
      url: 'https://radiant-gorge-59675.herokuapp.com/getresult',
      data: this.state.selectedAnswers
    }).then(res => {
      this.setState({
        totalRightAnswer: res.data.correctAnswerCount,
        totalMarks: res.data.correctAnswerCount * 2
      })
      this.toggleModal();
    });


  }
  renderButton = () => {
    return (
      <ButtonWrapper>
        <Button value="submit" onClick={this.showResult}>Submit</Button>
      </ButtonWrapper>
    )
  }
  componentWillMount() {
    const { match: { params } } = this.props;
    this.setState({
      step: parseInt(params.id)
    })
    //https://radiant-gorge-59675.herokuapp.com/getquestions
    axios.get('https://radiant-gorge-59675.herokuapp.com/getquestions')
      .then(res => {
        const questions = res.data.questions;
        this.setState({ questions });
      })

  }
  render() {
    const state = this.state;
    const step = state.step;
    const countperpage = state.countsperpage;
    const CarouselDots = Math.ceil(state.questions.length / countperpage);
    console.log(`dots count ${CarouselDots}`)
    return (
      <div className="App">
        {this.renderQuizData(state.questions)}
        <FooterWrapper>
          {step >= 2 && (
            <div>
              <div onClick={() => {
                this.setState({ step: state.step - 1 })
              }} >
                <Link to={`/${state.step - 1}`}>
                  <ChevImage src={LeftIcon} left></ChevImage>
                </Link>
              </div>
            </div>
          )}
          <Dots>
            {
              Array.from(Array(CarouselDots), (_, index) => <Dot key={index}>
                {index === step - 1 ? '● ' : '○ '}
              </Dot>)
            }
          </Dots>
          {
            (countperpage * step) < state.questions.length &&
            <div onClick={() => {
              this.setState({ step: state.step + 1 })
            }} >
              <Link to={`/${state.step + 1}`}>
                <ChevImage src={RightIcon} right></ChevImage>
              </Link>
            </div>
          }
          {(countperpage * step) >= state.questions.length && state.questions.length > 0 ? this.renderButton() : ''}
        </FooterWrapper>
        <br />
        <Modal show={state.isOpen}
          onClose={this.toggleModal}
          totalMarks={state.totalMarks}
          totalRightAnswer={state.totalRightAnswer}
          totalQuestion={state.questions.length}
        >
        </Modal>
      </div>
    );
  }
}

export default App;
